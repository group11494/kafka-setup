https://chatgpt.com/c/7c7a8d6a-9973-4ccf-9030-b29c039fc983


source kafka_ssl_env/bin/activate  # Activate the virtual environment

cd ssl
vi test3.py
python /home/rinkirathore/ssl/test3.py
// to run file with actual path 

(kafka_ssl_env) rinkirathore@QB-NOI-1052:~/ssl$ cat test3.py 

# extract ca file from client trust store as pem key 
# keytool -exportcert -alias CARoot -keystore kafka.client.truststore.jks -storepass clientpass -file kafka-ca-cert.pem
# chmod 644 /home/rinkirathore/ssl/kafka-ca-cert.pem  # Ensure it is readable by the user
#  file /home/rinkirathore/ssl/kafka-ca-cert.pem


#1 Check if the File is a DER Certificate:
# openssl x509 -inform DER -in /home/rinkirathore/ssl/kafka-ca-cert.pem -noout -text

# 2 Convert the DER Certificate to PEM Format
# openssl x509 -inform DER -in /home/rinkirathore/ssl/kafka-ca-cert.pem -out /home/rinkirathore/ssl/kafka-ca-cert.pem -outform PEM

# 3 After converting, verify that the file is now in PEM format:
# openssl x509 -in /home/rinkirathore/ssl/kafka-ca-cert.pem -noout -text

# Now test if cert is accessible and then run the script


from confluent_kafka import Producer, Consumer, KafkaException
import os

# Kafka broker properties
bootstrap_servers = 'localhost:9093'  # Replace with your Kafka broker's hostname and port
topic = 'kafka-security-topic'

# Absolute path to the extracted CA certificate in PEM format
ca_cert_path = '/home/rinkirathore/ssl/kafka-ca-cert.pem'

# Verify the file exists and print its content as bytes
if not os.path.isfile(ca_cert_path):
    raise FileNotFoundError(f"The certificate file at {ca_cert_path} was not found.")

with open(ca_cert_path, 'rb') as cert_file:
    cert_content = cert_file.read()
    print("Certificate file content (as bytes):\n", cert_content)

# Kafka producer configuration with SSL
producer_conf = {
    'bootstrap.servers': bootstrap_servers,
    'security.protocol': 'SSL',
    'ssl.ca.location': ca_cert_path,
    'ssl.key.password': 'clientpass',  # Password for client's key if using client authentication
}

# Kafka consumer configuration with SSL
consumer_conf = {
    'bootstrap.servers': bootstrap_servers,
    'group.id': 'test-group',
    'security.protocol': 'SSL',
    'ssl.ca.location': ca_cert_path,
    'ssl.key.password': 'clientpass',  # Password for client's key if using client authentication
    'auto.offset.reset': 'earliest',
}

# Example producer usage
p = None
try:
    p = Producer(producer_conf)
    while True:
        # Read input from user
        msg = input("Enter message to send (or 'exit' to quit): ")
        if msg.lower() == 'exit':
            break
        # Produce message to Kafka
        p.produce(topic, value=msg.encode('utf-8'))
        p.flush()

except KeyboardInterrupt:
    pass
except KafkaException as e:
    print(f"Exception while producing to Kafka: {e}")

finally:
    if p is not None:
        # Since 'close()' might not be available, perform necessary cleanup if any
        p.flush(30)  # Wait up to 30 seconds for events to be delivered

# Example consumer usage
c = None
try:
    c = Consumer(consumer_conf)
    c.subscribe([topic])
    while True:
        msg = c.poll(timeout=1.0)
        if msg is None:
            continue
        if msg.error():
            print(f"Consumer error: {msg.error()}")
            continue
        print(f"Received message: {msg.value().decode('utf-8')}")

except KeyboardInterrupt:
    pass
except KafkaException as e:
    print(f"Exception in consumer loop: {e}")

finally:
    if c is not None:
        c.close()
