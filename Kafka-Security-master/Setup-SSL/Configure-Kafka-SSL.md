step 2




## create key store ....a server certificate !! put your public EC2-DNS here, after "CN="
```
export SRVPASS=serversecret
cd ssl
///// replace dname with your machine public ip on which kafka broker is setup and adding -keyalg RSA and this generates keystore with 'mykey' entry in it.
keytool -genkey -keystore kafka.server.keystore.jks -validity 365 -storepass $SRVPASS -keypass $SRVPASS  -dname "CN=localhost" -storetype pkcs12 -keyalg RSA

keytool -genkey -keystore kafka.server.keystore.jks -validity 365 -storepass $SRVPASS -keypass $SRVPASS  -dname "CN=ec2-18-196-169-2.eu-central-1.compute.amazonaws.com" -storetype pkcs12 -keyalg RSA
#> ll

keytool -list -v -keystore kafka.server.keystore.jks
```

## create a cert-file... certification request file, to be signed by the CA, generates cert-file
```
keytool -keystore kafka.server.keystore.jks -certreq -file cert-file -storepass $SRVPASS -keypass $SRVPASS
#> ll
```

##  output: file "cert-signed" and ca-cert.srl ie sign the server certificate =>
```
openssl x509 -req -CA ca-cert -CAkey ca-key -in cert-file -out cert-signed -days 365 -CAcreateserial -passin pass:$SRVPASS
#> ll
```

## check certificates
### our local certificates
```
keytool -printcert -v -file cert-signed
keytool -list -v -keystore kafka.server.keystore.jks
```
till now key store has only mykey with 1 point extension


# Trust the CA by creating a truststore and importing the ca-cert, **creates a trust store**
```
keytool -keystore kafka.server.truststore.jks -alias CARoot -import -file ca-cert -storepass $SRVPASS -keypass $SRVPASS -noprompt -keyalg RSA

keytool -list -v -keystore kafka.server.truststore.jks


```
this trust store has one ca rppt cert with 3 points
# Import CA and the signed server certificate into the keystore. 
key stre had only mykey now it after firstm, it will have caroot as well with 3 points 1 2 3 
after second step, key store me pehle bas my key ki 1 point tha ab mykey ke 3 point ho gaye add. so after two steps, key store me my key and caroot ke 3 -3 points honge
```
keytool -keystore kafka.server.keystore.jks -alias CARoot -import -file ca-cert -storepass $SRVPASS -keypass $SRVPASS -noprompt
keytool -keystore kafka.server.keystore.jks -import -file cert-signed -storepass $SRVPASS -keypass $SRVPASS -noprompt
```

# Adjust Broker configuration  
Replace the server.properties in AWS, by using [this one](./server.properties).   
*Ensure that you set your public-DNS* !!

# Restart Kafka
```
sudo systemctl restart kafka
sudo systemctl status kafka  
```
# Verify Broker startup
```
sudo grep "EndPoint" /home/ubuntu/kafka/logs/server.log
```
or 
```
sudo grep "EndPoint" /home/rinkirathore/kafka/logs/server.log
```
# Adjust SecurityGroup to open port 9093

# Verify SSL config
from your local computer
```
openssl s_client -connect <<your-public-DNS>>:9093
```
or 
```
openssl s_client -connect localhost:9093 -cipher ALL
```
